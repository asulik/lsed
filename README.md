# Statistical Machine Learning Course Tasks Repository

Welcome to the Statistical Machine Learning Course Tasks Repository!

This repository contains my solutions to various tasks assigned during a comprehensive statistical machine learning course. The covered topics include but are not limited to:
**Principal Component Analysis (PCA)**,
**Support Vector Machines (SVM)**,
**k-Nearest Neighbors (k-nn)**,
**Linear Discriminant Analysis (LDA)**,

And more!

Please note that all Jupyter notebooks within this repository are written in Polish, as the course was conducted in the Polish language.

Feel free to explore the notebooks for insights into the implementation and application of these machine learning algorithms. Whether you are a student seeking learning resources or a practitioner looking for practical examples, these solutions offer a valuable resource for understanding statistical machine learning concepts. Happy learning!
